<?php /*%%SmartyHeaderCode:11222596e35cea65491-02477299%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '81e1fd981493037f6aaec27de0860417957ab7c0' => 
    array (
      0 => '.\\templates\\about_us.tpl',
      1 => 1500394693,
      2 => 'file',
    ),
    '10e0737838b4a574ef135d0c601e7b602cfaf37a' => 
    array (
      0 => '.\\templates\\header.tpl',
      1 => 1500392904,
      2 => 'file',
    ),
    '78dfd564017866b2a07d32220e5f711a454b3ecb' => 
    array (
      0 => '.\\templates\\nav.tpl',
      1 => 1500395015,
      2 => 'file',
    ),
    '1be7ff7fdee636597edd726ee98dfef4bfd55d1f' => 
    array (
      0 => '.\\templates\\footer.tpl',
      1 => 1500382255,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11222596e35cea65491-02477299',
  'has_nocache_code' => false,
  'cache_lifetime' => 120,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!$no_render) {?><HTML>
<HEAD>
<TITLE>About Us </TITLE>

 <style type="text/css">
   body{
   font-family:Arial, Helvetica, sans-serif;
   font-size:12px;
   color:#333333;
   background-color:azure;
   }
          
 </style>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</HEAD>
<BODY bgcolor="skyblue">
 <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">DreamSite</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="index.php">Home</a></li>
      <li><a href="about_us.php">About Us</a></li>
      <li><a href="products.php">Products</a></li>
      <li><a href="services.php">Services</a></li>
            <li><a href="contact.php">Contact Us</a></li>

    </ul>
  </div>
</nav>  
<div class="container">
  <h3>Who We Are...</h3>
  
 <p>About Content Marketing

Content marketing involves the creation and distribution of relevant, informative, and/or entertaining material in order to drive lead generation and revenue. This can include blogs, videos, infographics, podcasts, webinars, social media posts, and even movies.

Unlike advertising, content marketing does not interrupt the consumption of other content, such as a tv show, radio show, or news article. Content marketing is the content. It does not explicitly promote a brand, but is intended to stimulate interest in an organization’s products, services, or industry.

This blog is a forum for marketers to learn and share best practices and tips across all facets of content marketing: strategy, creation, promotion/distribution, and analytics. If you’re interested in writing for Curata, please read our style guidelines. 

We offer a weekly newsletter focused on all things content marketing, free to all subscribers. You are also welcome to participate in our Content Marketing Forum LinkedIn Community.

Learn how we help companies from startups to the largest companies in the world power their content marketing via the Curata web site. Or speak to one of our sales representatives.</p>

</div>

</BODY>
</HTML>
<?php } ?>