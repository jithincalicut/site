<?php
require('libs/Smarty.class.php');

$smarty = new Smarty;



//$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = true;
$smarty->cache_lifetime = 120;


$smarty->assign("title", "About Us");
$smarty->assign("header", "Who We Are...");
$smarty->assign("page_content", "About Content Marketing

Content marketing involves the creation and distribution of relevant, informative, and/or entertaining material in order to drive lead generation and revenue. This can include blogs, videos, infographics, podcasts, webinars, social media posts, and even movies.

Unlike advertising, content marketing does not interrupt the consumption of other content, such as a tv show, radio show, or news article. Content marketing is the content. It does not explicitly promote a brand, but is intended to stimulate interest in an organization’s products, services, or industry.

This blog is a forum for marketers to learn and share best practices and tips across all facets of content marketing: strategy, creation, promotion/distribution, and analytics. If you’re interested in writing for Curata, please read our style guidelines. 

We offer a weekly newsletter focused on all things content marketing, free to all subscribers. You are also welcome to participate in our Content Marketing Forum LinkedIn Community.

Learn how we help companies from startups to the largest companies in the world power their content marketing via the Curata web site. Or speak to one of our sales representatives.");
$smarty->assign("link", "DreamSite");
$smarty->assign("list1", "Home");
$smarty->assign("list2", "About Us");
$smarty->assign("list3", "Products");
$smarty->assign("list4", "Services");
$smarty->assign("list5", "Contact Us");

$smarty->display('about_us.tpl');
?>
