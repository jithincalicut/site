<?php /* Smarty version Smarty-3.0.8, created on 2017-07-18 16:12:48
         compiled from ".\templates\contact_us.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2330596e3380a4c895-04239201%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1f6a35a956a6389277fccf60ae543076470e49c1' => 
    array (
      0 => '.\\templates\\contact_us.tpl',
      1 => 1500394363,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2330596e3380a4c895-04239201',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $sha = sha1("header.tpl" . $_smarty_tpl->cache_id . $_smarty_tpl->compile_id);
if (isset($_smarty_tpl->smarty->template_objects[$sha])) {
$_template = $_smarty_tpl->smarty->template_objects[$sha]; $_template->caching = 9999; $_template->cache_lifetime =  null;
} else {
$_template = new Smarty_Internal_Template("header.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null);
}
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<?php $sha = sha1("nav.tpl" . $_smarty_tpl->cache_id . $_smarty_tpl->compile_id);
if (isset($_smarty_tpl->smarty->template_objects[$sha])) {
$_template = $_smarty_tpl->smarty->template_objects[$sha]; $_template->caching = 9999; $_template->cache_lifetime =  null;
} else {
$_template = new Smarty_Internal_Template("nav.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null);
}
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

  <br><br><br><br>
      
    <div class="container">  
        <div class="row">  
            <div class="col-md-4 col-md-offset-4">  
                <div class="login-panel panel panel-success">  
                    <div class="panel-heading">  
                        <h3 class="panel-title">Leave us a Message</h3>  
                    </div>  
                    <div class="panel-body">  
                        <form role="form" method="post" action="contact_us.php">  
                            <fieldset>  
                                <div class="form-group"  >  
                                    <input class="form-control" name="name" type="text" autofocus placeholder="Name" required />  
                                </div>  
                                <div class="form-group">  
                                    <input class="form-control" placeholder="Email" name="email" type="email" value="" required />  
                                </div>  
      
      <div class="form-group">  
                                    <input class="form-control" placeholder="Phone" name="phone" type="text" value="" required />  
                                </div>  


                                <div class="form-group">  
                                    <textarea cols="46" rows="4" placeholder="Message" name="message" type="text" value="" required></textarea>  
                                </div>  

      
                                <input class="btn btn-lg btn-success btn-block" type="submit" value="Send" name="send" >  
      
      
                            </fieldset>  
                        </form>  
                    </div>  
                </div>  
            </div>  
        </div>  
    </div>  
      <?php $sha = sha1("footer.tpl" . $_smarty_tpl->cache_id . $_smarty_tpl->compile_id);
if (isset($_smarty_tpl->smarty->template_objects[$sha])) {
$_template = $_smarty_tpl->smarty->template_objects[$sha]; $_template->caching = 9999; $_template->cache_lifetime =  null;
} else {
$_template = new Smarty_Internal_Template("footer.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null);
}
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
