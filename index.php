<?php
require('libs/Smarty.class.php');

$smarty = new Smarty;



//$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = true;
$smarty->cache_lifetime = 120;


$smarty->assign("title", "Home");
$smarty->assign("header", "Welcome to my website");
$smarty->assign("page_content", "Let me wish you a warm welcome to my official personal website. Here you can find information on research work, supervision, publication and other activities. Apart from this official website You can share informations and submit your queries through feedback form. We've all heard how important it is to make a good first impression. Show up late for a job interview? That's a bad first impression. Eat a ton of garlic and forget to brush your teeth right before a first date? Also a bad first impression. Go to meet your significant others' parents for the first time dressed in Crocs and sweatpants? That might also result in a bad first impression (depending on prevailing fashion sensibilities).

It turns out that the make a good first impression principle holds true not only in face-to-face encounters, but in email interactions as well.
Click here to download our free template for planning and tracking your email marketing efforts.

When you send off a welcome email to a new blog or newsletter subscriber, or to a new customer, you're making a first impression on behalf of your brand.

To help ensure you're making the best first impression possible, we've rounded up some examples of standout welcome emails from brands big and small. As you'll soon discover, each example showcases different tactics and strategies for engaging new email subscribers. Let's dive in ...");
$smarty->assign("link", "DreamSite");
$smarty->assign("list1", "Home");
$smarty->assign("list2", "About Us");
$smarty->assign("list3", "Products");
$smarty->assign("list4", "Services");
$smarty->assign("list5", "Contact Us");

$smarty->display('index.tpl');
?>
