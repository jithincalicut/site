<?php
require('libs/Smarty.class.php');

$smarty = new Smarty;




$smarty->caching = true;
$smarty->cache_lifetime = 120;


$smarty->assign("title", "Contact Us");
$smarty->assign("header", "Leave us a message");
$smarty->assign("link", "DreamSite");
$smarty->assign("list1", "Home");
$smarty->assign("list2", "About Us");
$smarty->assign("list3", "Products");
$smarty->assign("list4", "Services");
$smarty->assign("list5", "Contact Us");


$smarty->display('contact_us.tpl');
?>
