<?php
require('libs/Smarty.class.php');

$smarty = new Smarty;



//$smarty->force_compile = true;
//$smarty->debugging = true;
$smarty->caching = true;
$smarty->cache_lifetime = 120;


$smarty->assign("title", "Services");
$smarty->assign("header", "Our Services");
$smarty->assign("page", "Our Services include: ");

$smarty->assign("link", "DreamSite");
$smarty->assign("list1", "Home");
$smarty->assign("list2", "About Us");
$smarty->assign("list3", "Products");
$smarty->assign("list4", "Services");
$smarty->assign("list5", "Contact Us");

$smarty->display('services.tpl');
?>
