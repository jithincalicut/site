{include file="header.tpl"}

{include file="nav.tpl"}

  <br><br><br><br>
      
    <div class="container">  
        <div class="row">  
            <div class="col-md-4 col-md-offset-4">  
                <div class="login-panel panel panel-success">  
                    <div class="panel-heading">  
                        <h3 class="panel-title">Leave us a Message</h3>  
                    </div>  
                    <div class="panel-body">  
                        <form role="form" method="post" action="contact_us.php">  
                            <fieldset>  
                                <div class="form-group"  >  
                                    <input class="form-control" name="name" type="text" autofocus placeholder="Name" required />  
                                </div>  
                                <div class="form-group">  
                                    <input class="form-control" placeholder="Email" name="email" type="email" value="" required />  
                                </div>  
      
      <div class="form-group">  
                                    <input class="form-control" placeholder="Phone" name="phone" type="text" value="" required />  
                                </div>  


                                <div class="form-group">  
                                    <textarea cols="46" rows="4" placeholder="Message" name="message" type="text" value="" required></textarea>  
                                </div>  

      
                                <input class="btn btn-lg btn-success btn-block" type="submit" value="Send" name="send" >  
      
      
                            </fieldset>  
                        </form>  
                    </div>  
                </div>  
            </div>  
        </div>  
    </div>  
      {include file="footer.tpl"}
